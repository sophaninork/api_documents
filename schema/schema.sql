CREATE TABLE `users` (
	`id` smallint(6) NOT NULL AUTO_INCREMENT,
    	`name` varchar(100) NOT NULL,
    	`profile` varchar(64) DEFAULT NULL,
    	`email` varchar(500) NOT NULL,
    	`email_verified_at` datetime DEFAULT NULL,
    	`password` varchar(100) NOT NULL,
    	`status` ENUM('active', 'inactive') NOT NULL DEFAULT 'active',
    	`remember_token` varchar(100) DEFAULT NULL,
    	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    	`updated_at` timestamp NULL DEFAULT NULL,
    	`access_token` varchar(255) DEFAULT NULL,
    	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `sessions` (
	`id` varchar(191) NOT NULL,
	`user_id` smallint(6) DEFAULT NULL,
	`ip_address` varchar(45) DEFAULT NULL,
	`user_agent` text,
	`payload` text NOT NULL,
	`last_activity` int(11) NOT NULL,
	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `api_logs` (
	`id` int(11) NOT NULL,
	`sid` varchar(16) NOT NULL,
	`session_id` varchar(191) DEFAULT NULL,
	`required_json` json NULL,
	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`update_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `api_logs` ADD `url` varchar(500) NULL AFTER `session_id`,
ADD `request_header` JSON NULL AFTER `url`;

CREATE TABLE `schools` (
	`id` smallint(6) NOT NULL AUTO_INCREMENT,
	`name` varchar(256) NOT NULL,
	`image` varchar(256) DEFAULT NULL,
	`address` varchar(256) NOT NULL,
	`status` ENUM('publish', 'unpublish') NOT NULL DEFAULT 'publish',
	`deleted_at` timestamp NULL DEFAULT NULL,
	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `departments` (
	`id` int(10) NOT NULL AUTO_INCREMENT,
	`school_id` smallint(6) NOT NULL,
	`name` varchar(256) NOT NULL,
	`image` varchar(256) DEFAULT NULL,
	`tel` varchar(100) DEFAULT NULL,
	`email` varchar(256) NOT NULL,
	`status` ENUM('publish', 'unpublish') NOT NULL DEFAULT 'publish',
	`deleted_at` timestamp NULL DEFAULT NULL,
	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `posts` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`user_id` smallint(6) NOT NULL,
	`department_id` smallint(6) NOT NULL,
	`title` varchar(256) NOT NULL,
	`image` varchar(256) NOT NULL,
	`body` varchar(8000) NOT NULL,
	`status` ENUM('publish', 'unpublish') NOT NULL DEFAULT 'publish',
	`deleted_at` timestamp NULL DEFAULT NULL,
	`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY(`id`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

#10-1-2020
ALTER TABLE `posts` MODIFY `image` varchar(256);
ALTER TABLE `posts` MODIFY `body` varchar(8000);